package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

public class Background extends JPanel {
	
	Timer fpsTimer = new Timer(1000,null);
	int fps;
	int fpsCount;
	GameLoop gl;
	
	public Background(GameLoop gl) {
		this.gl = gl;
		fpsTimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fps = fpsCount;
				fpsCount = 0;
				//lblFps.setText("FPS: " + fps);
				repaint();
				System.out.println("111");
			}
		});
		
		fpsTimer.start();
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		System.out.println(gl.sizeX + "/" + gl.sizeY);
		setBounds(0, 0, gl.sizeX, gl.sizeY);
		g2d.fillRect(0, 0, gl.sizeX, gl.sizeY);
		g2d.setColor(Color.WHITE);
		g2d.setFont(new Font("Tahoma", Font.BOLD, 20));
		g2d.drawString("FPS: " + fps, 15, 25);
		ImageIcon img = new ImageIcon(GameLoop.class.getResource("/img/playButton.png"));
		//repaint();
		fpsCount++;
	}
}
