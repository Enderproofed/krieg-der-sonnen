package main;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.Timer;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GameLoop extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	Timer gameLoopTimer;
	Background bg;
	JLabel lblTitel;
	JLabel lblPlayButton;
	int sizeX, sizeY;
	JFrame frame;
	
	public GameLoop(JFrame frame) {
		this.frame = frame;
		//setBackground(new Color(240, 240, 240));
		setFocusable(true);
		sizeX = frame.getSize().width;
		sizeY = frame.getSize().height;
		
		gameLoopTimer = new Timer(1,this);
		setLayout(null);
		gameLoopTimer.start();
		
		lblTitel = new JLabel("Krieg der Sonnen");
		//lblTitel.setIcon(new ImageIcon(GameLoop.class.getResource("/img/playButton.png")));
		
		lblTitel.setForeground(Color.WHITE);
		lblTitel.setFont(new Font("Tahoma", Font.BOLD, 50));
		lblTitel.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitel.setBounds(0, 0, sizeX, 96);
		add(lblTitel);
		
		lblPlayButton = new JLabel("");
		lblPlayButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(1);
			}
		});
		lblPlayButton.setIcon(new ImageIcon(GameLoop.class.getResource("/img/playButton.png")));
		lblPlayButton.setBounds(sizeX/2-50, 200, 100, 100);
		add(lblPlayButton);
		
		bg = new Background(this);
		bg.setBounds(0, 0, sizeX, sizeY);
		add(bg);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// GAME LOOP
		
		sizeX = frame.getSize().width;
		sizeY = frame.getSize().height;
		lblPlayButton.setBounds(sizeX/2-50, 200, 100, 100);
		lblTitel.setBounds(0, 0, sizeX, 96);
		System.out.println(sizeX + "/" + sizeY);
		repaint();
	}
}

